import React from "react";
import { SubFormComponentForKey2DataType } from "./";

export const ArrayOfSubFormComponentsForKey3DataType = props => {
  const handleFieldChange = index => (event, value, selectedKey) => {
    let data = [...props.value];
    data[index] = value;
    props.onChange(null, data);
  };

  return (
    <div>
      {props.value.map((subform, index) => (
        <SubFormComponentForKey2DataType
          key={index}
          value={subform}
          onChange={handleFieldChange(index)}
        />
      ))}
    </div>
  );
};
