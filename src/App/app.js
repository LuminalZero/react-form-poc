import React, { Component } from 'react';
import './app.css';
import { ArrayOfSubFormComponentsForKey3DataType, SubFormComponentForKey2DataType } from './components';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {
        key1: "key1",
        key2: { key: "a" },
        key3: [{ key: "b" }, { key: "c" }]
      }
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
  }

  handleFieldChange = field => (event, value, selectedKey) => {
    // make a copy of the object first to avoid changes by reference
    let data = {
      ...this.state.data
    };

    // use here event or value of selectedKey depending on your component's event
    data[field] = value;

    this.setState({
      data
    });
  };

  handleSubmit = event => {
    event.preventDefault();

    this.setState(prevState => ({
      data: prevState.data || {}
    }));
  };

  render() {
    return (
      <div className="App">
        <h1>Test Form</h1>
        <div className="App-container">
          <div>{JSON.stringify(this.state.data)}</div>
          <br />
          <br />
          <form onSubmit={this.handleSubmit}>
            <input
              type="text"
              name="key1"
              onChange={this.handleFieldChange("key1")}
              value={this.state.data.key1}
            />
            <br />
            <br />
            <SubFormComponentForKey2DataType
              onChange={this.handleFieldChange("key2")}
              value={this.state.data.key2}
            />
            <ArrayOfSubFormComponentsForKey3DataType
              onChange={this.handleFieldChange("key3")}
              value={this.state.data.key3}
            />
            <button type="submit">Submit</button>
          </form>
        </div>
      </div>
    );
  }
}
