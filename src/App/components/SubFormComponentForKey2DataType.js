import React from "react";

export const SubFormComponentForKey2DataType = props => {
  const handleFieldChamge = field => (event, value, selectedKey) => {
    debugger;
    let data = { ...props.value };
    data[field] = value;
    // you could pass the event here but also null if it is not necessary nor useful
    props.onChange(null, data);
  };

  return (
    <React.Fragment>
      <input type="text" onChange={handleFieldChamge} value={props.value} />
      <br />
      <br />
    </React.Fragment>
  );
};
